# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturinig 2018-2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import socket
import struct
import logging

import numpy as np

from . import ForceTorqueI


class EmikaFTEstimator(ForceTorqueI):
    def __init__(self, emika_gw_host, emika_gw_port=10000, timeout=0.01,
                 bind_host='0.0.0.0'):
        ForceTorqueI.__init__(self)
        self._log = logging.getLogger(
            f'{self.__class__.__name__}@{emika_gw_host}:{emika_gw_port}')
        self._emika_gw_addr = (emika_gw_host, emika_gw_port)
        self._bind_addr = (bind_host, emika_gw_port)
        self._sock = socket.socket(type=socket.SOCK_DGRAM)
        self._sock.settimeout(timeout)
        self._sock.bind(self._bind_addr)
        self._w_struct = struct.Struct('6d')
        self.start_stream()

    def _recv_ft(self):
        try:
            # Note that Emika gives the FT which it exerts, whereas a
            # FT sensor is expected to tell the FT which it
            # experiences. Hence the minus sign.
            new_ft = -np.array(self._w_struct.unpack(self._sock.recv(1024)))
            with self._ft_lock:
                self._latest_ft = new_ft
        except socket.timeout:
            self._log.warning('Timeout during reception of data.')
            with self._ft_lock:
                self._latest_ft = None

    def _req_single_ft(self):
        self.wait_for_data()
